#include <locale.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../headers/graphics.h"
#include "../headers/args.h"

int main(int argc, char *argv[])
{
   setlocale(LC_ALL, "");
   initscr();
   curs_set(0);

   int help = 0; /* Whether or not the help screen should appear */
   help = help_arg(argc, argv);
   if (help == 1)
   {
      help_scr();
      endwin();
      return 0;
   }


   int driver_used = 0; /* This is 0 if neither radeon nor amdgpu are being used. 1 for radeon. 2 for amdgpu */
   int shadow_sprite = 0; /* 0 for no shadows on the sprite. 1 for sprites with shadows */
   int bgcolor = COLOR_BLACK;
   int fgcolor = COLOR_WHITE;

   shadow_sprite = shadow_arg(argc, argv);
   bgcolor = bgcolor_arg(argc, argv);
   fgcolor = fgcolor_arg(argc, argv);
   set_colors(bgcolor, fgcolor);

   system("lspci -k | grep radeon > /tmp/drivercheck_TMPFILE_69420.txt");
   FILE *tmp_file = fopen("/tmp/drivercheck_TMPFILE_69420.txt", "r");
   char *buffer = malloc(62);
   fread(buffer, 1, 62, tmp_file);

   if (strstr(buffer, "Kernel driver in use: radeon") != NULL)
   {
      driver_used = 1;
   }

   else
   {
      fclose(tmp_file);
      system("lspci -k | grep amdgpu > /tmp/drivercheck_TMPFILE_69420.txt");
      tmp_file = fopen("/tmp/drivercheck_TMPFILE_69420.txt", "r");
      fread(buffer, 1, 62, tmp_file);

      if (strstr(buffer, "Kernel driver in use: amdgpu") != NULL) {
            driver_used = 2;
      }
   }

   free(buffer);
   fclose(tmp_file);
   remove("/tmp/drivercheck_TMPFILE_69420.txt");

   if (driver_used == 1)
   {
      if (shadow_sprite == 0) {
         draw_radeon_scr();
      }

      else {
         draw_radeon_scr_shadow();
      }
   }

   else if (driver_used == 2)
   {
      if (shadow_sprite == 0) {
         draw_amdgpu_scr();
      }

      else {
         draw_amdgpu_scr_shadow();
      }
   }

   else
   {
      if (shadow_sprite == 0) {
         draw_unknown_scr();
      }

      else {
         draw_unkown_scr_shadow();
      }
   }

   endwin();
   return 0;
}
