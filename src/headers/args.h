#include <string.h>

/* Checks for the --shadow argument. Returns 1 if found. 0 if not found */
int shadow_arg(int argc, char *argv[]);

/* Checks for the background color. Returns 0 if no colors are found */
int bgcolor_arg(int argc, char *argv[]);
/* Checks for the foreground color. Returns 7 if no colors are found */
int fgcolor_arg(int argc, char *argv[]);

/* Checks for the --help argument. Returns 1 if it finds it. Returns 0 if it doesn't */
int help_arg(int argc, char *argv[]);
