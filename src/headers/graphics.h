#ifndef _GRAPHICS_H
#define _GRAPHICS_H

#include <ncurses.h>

/* "no-shadow" sprites */
void draw_radeon_scr();
void draw_amdgpu_scr();
void draw_unknown_scr();

/* Shadowed sprites */
void draw_radeon_scr_shadow();
void draw_amdgpu_scr_shadow();
void draw_unkown_scr_shadow();

/* Handles color */
void set_colors(int bgcolor, int fgcolor);

/* Draws the help screen */
void help_scr();

#endif
