#include "../headers/graphics.h"

void draw_radeon_scr()
{
   move(LINES /2 -2, COLS /2 -25);
   printw("██████   █████  ██████  ███████  ██████  ███    ██");
   move(LINES /2 -1, COLS /2 -25);
   printw("██   ██ ██   ██ ██   ██ ██      ██    ██ ████   ██");
   move(LINES /2, COLS /2 -25);
   printw("██████  ███████ ██   ██ █████   ██    ██ ██ ██  ██");
   move(LINES /2 +1, COLS /2 -25);
   printw("██   ██ ██   ██ ██   ██ ██      ██    ██ ██  ██ ██");
   move(LINES /2 +2, COLS /2 -25);
   printw("██   ██ ██   ██ ██████  ███████  ██████  ██   ████");

   getch();
}

void draw_amdgpu_scr()
{
   move(LINES /2 -2, COLS /2 -26);
   printw(" █████  ███    ███ ██████   ██████  ██████  ██    ██");
   move(LINES /2 -1, COLS /2 -26);
   printw("██   ██ ████  ████ ██   ██ ██       ██   ██ ██    ██");
   move(LINES /2, COLS /2 -26);
   printw("███████ ██ ████ ██ ██   ██ ██   ███ ██████  ██    ██");
   move(LINES /2 +1, COLS /2 -26);
   printw("██   ██ ██  ██  ██ ██   ██ ██    ██ ██      ██    ██");
   move(LINES /2 +2, COLS /2 -26);
   printw("██   ██ ██      ██ ██████   ██████  ██       ██████ ");

   getch();
}

void draw_unknown_scr()
{
   move(LINES /2 -2, COLS /2 -3);
   printw("██████ ");
   move(LINES /2 -1, COLS /2 -3);
   printw("     ██");
   move(LINES /2, COLS /2 -3);
   printw("  ▄███ ");
   move(LINES /2 +1, COLS /2 -3);
   printw("  ▀▀   ");
   move(LINES /2 +2, COLS /2 -3);
   printw("  ██   ");

   getch();
}

void draw_radeon_scr_shadow()
{
   move(LINES /2 -2, COLS /2 -25);
   printw("██████╗  █████╗ ██████╗ ███████╗ ██████╗ ███╗   ██╗");
   move(LINES /2 -1, COLS /2 -25);
   printw("██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔═══██╗████╗  ██║");
   move(LINES /2, COLS /2 -25);
   printw("██████╔╝███████║██║  ██║█████╗  ██║   ██║██╔██╗ ██║");
   move(LINES /2 +1, COLS /2 -25);
   printw("██╔══██╗██╔══██║██║  ██║██╔══╝  ██║   ██║██║╚██╗██║");
   move(LINES /2 +2, COLS /2 -25);
   printw("██║  ██║██║  ██║██████╔╝███████╗╚██████╔╝██║ ╚████║");
   move(LINES /2 +3, COLS /2 -25);
   printw("╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝");

   getch();
}

void draw_amdgpu_scr_shadow()
{
   move(LINES /2 -2, COLS /2 -26);
   printw(" █████╗ ███╗   ███╗██████╗  ██████╗ ██████╗ ██╗   ██╗");
   move(LINES /2 -1, COLS /2 -26);
   printw("██╔══██╗████╗ ████║██╔══██╗██╔════╝ ██╔══██╗██║   ██║");
   move(LINES /2, COLS /2 -26);
   printw("███████║██╔████╔██║██║  ██║██║  ███╗██████╔╝██║   ██║");
   move(LINES /2 +1, COLS /2 -26);
   printw("██╔══██║██║╚██╔╝██║██║  ██║██║   ██║██╔═══╝ ██║   ██║");
   move(LINES /2 +2, COLS /2 -26);
   printw("██║  ██║██║ ╚═╝ ██║██████╔╝╚██████╔╝██║     ╚██████╔╝");
   move(LINES /2 +3, COLS /2 -26);
   printw("╚═╝  ╚═╝╚═╝     ╚═╝╚═════╝  ╚═════╝ ╚═╝      ╚═════╝ ");

   getch();  
}

void draw_unkown_scr_shadow()
{
   move(LINES /2 -2, COLS /2 -3);
   printw("██████╗ ");
   move(LINES /2 -1, COLS /2 -3);
   printw("╚════██╗");
   move(LINES /2, COLS /2 -3);
   printw("  ▄███╔╝");
   move(LINES /2 +1, COLS /2 -3);
   printw("  ▀▀══╝ ");
   move(LINES /2 +2, COLS /2 -3);
   printw("  ██╗   ");
   move(LINES /2 +3, COLS /2 -3);
   printw("  ╚═╝   ");

   getch();
}

void set_colors(int bgcolor, int fgcolor)
{
   if (bgcolor != COLOR_BLACK || fgcolor != COLOR_WHITE)
   {
      start_color();
      init_pair(1, fgcolor, bgcolor);
      bkgdset(COLOR_PAIR(1));
      clear();
   }
}

void help_scr()
{
   move(LINES /2 -6, COLS /2 -5);
   printw("ARGUMENTS:");
   
   move(LINES /2 -4, COLS /2 -19);
   printw("--shadow: Adds shading to the font");
   move(LINES /2 -2, COLS /2 -19);
   printw("-fg={color}: Changes the foreground color");
   move(LINES /2 -1, COLS /2 -19);
   printw("-bg={color}: Changes the background color");

   move(LINES /2 +1, COLS /2 -23);
   printw("--foreground={color}: Changes the foreground color");
   move(LINES /2 +2, COLS /2 -23);
   printw("--background={color}: Changes the background color");

   getch();
}
