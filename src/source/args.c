#include "../headers/args.h"

int shadow_arg(int argc, char *argv[])
{
   for (int i = 0; i < argc; i++)
   {
      if (strstr(argv[i], "--shadow") != NULL) {
         return 1;
      }
   }

   return 0;
}

int bgcolor_arg(int argc, char *argv[])
{
   for (int i = 0; i < argc; i++)
   {
      if (strstr(argv[i], "-bg=black") != NULL || strstr(argv[i], "--background=black") != NULL) {
         return 0;
      }

      else if (strstr(argv[i], "-bg=red") != NULL || strstr(argv[i], "--background=red") != NULL) {
         return 1;
      }

      else if (strstr(argv[i], "-bg=green") != NULL || strstr(argv[i], "--background=green") != NULL) {
         return 2;
      }
      
      else if (strstr(argv[i], "-bg=yellow") != NULL || strstr(argv[i], "--background=yellow") != NULL) {
         return 3;
      }

      else if (strstr(argv[i], "-bg=blue") != NULL || strstr(argv[i], "--background=blue") != NULL) {
         return 4;
      }

      else if (strstr(argv[i], "-bg=magenta") != NULL || strstr(argv[i], "--background=magenta") != NULL) {
         return 5;
      }

      else if (strstr(argv[i], "-bg=cyan") != NULL || strstr(argv[i], "--background=cyan") != NULL) {
         return 6;
      }

      else if (strstr(argv[i], "-bg=white") != NULL || strstr(argv[i], "--background=white") != NULL) {
         return 7;
      }
   }

   return 0;
}

int fgcolor_arg(int argc, char *argv[])
{
   for (int i = 0; i < argc; i++)
   {
      if (strstr(argv[i], "-fg=black") != NULL || strstr(argv[i], "--foreground=black") != NULL) {
         return 0;
      }

      else if (strstr(argv[i], "-fg=red") != NULL || strstr(argv[i], "--foreground=red") != NULL) {
         return 1;
      }

      else if (strstr(argv[i], "-fg=green") != NULL || strstr(argv[i], "--foreground=green") != NULL) {
         return 2;
      }
      
      else if (strstr(argv[i], "-fg=yellow") != NULL || strstr(argv[i], "--foreground=yellow") != NULL) {
         return 3;
      }

      else if (strstr(argv[i], "-fg=blue") != NULL || strstr(argv[i], "--foreground=blue") != NULL) {
         return 4;
      }

      else if (strstr(argv[i], "-fg=magenta") != NULL || strstr(argv[i], "--foreground=magenta") != NULL) {
         return 5;
      }

      else if (strstr(argv[i], "-fg=cyan") != NULL || strstr(argv[i], "--foreground=cyan") != NULL) {
         return 6;
      }

      else if (strstr(argv[i], "-fg=white") != NULL || strstr(argv[i], "--foreground=white") != NULL) {
         return 7;
      }
   }

   return 7;
}

int help_arg(int argc, char *argv[])
{
   for (int i = 0; i < argc; i++)
   {
      if (strstr(argv[i], "--help") != NULL || strstr(argv[i], "-h") != NULL) {
         return 1;
      }
   }

   return 0;
}
